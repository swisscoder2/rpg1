import Phaser from 'phaser';
import playerImg from './assets/char.png'
import mapJson from './assets/map2.json';
import tilesImg from './assets/tiles2.png';

class MyGame extends Phaser.Scene {
    constructor() {
        super();
    }

    preload() {
        //this.load.image('player', playerImg);      
        this.load.spritesheet('player', playerImg, { frameWidth: 64, frameHeight: 64 });
        this.load.tilemapTiledJSON('map', mapJson);
        console.log('map', mapJson);
        this.load.image('tiles', tilesImg);

    }

    create() {
        let map = this.make.tilemap({ key: 'map' });
        let tileset = map.addTilesetImage('roguelikeSheet_transparent', 'tiles');
        let floor = map.createLayer('Floor', tileset, 0, 0);
        // let details = map.createLayer('Details', tileset, 0, 0);
        // let objects = map.createLayer('Objects', tileset, 0, 0);
        // let carept = map.createLayer('Carpet', tileset, 0, 0);

        this.cameras.main.setBounds(0, 0, 50, 50);

        const playerAnimation = this.anims.create({
            key: 'walk',
            frames: this.anims.generateFrameNumbers('player', { frames: [ 48, 49, 50, 51, 52, 53] }),
            frameRate: 16,
            repeat: -1
        });

        //const sprite = this.add.sprite(50, 300, 'player').setScale(2);        

        this.player = this.physics.add.sprite(200, 200, 'player')
            .setScale(2);

        //this.player.play({ key: 'walk', repeat: 7 });


        this.cameras.main.startFollow(this.player, false, 0.2, 0.2);
        this.scale.setZoom(3);
        map.setCollisionBetween(150, 1000);

        // let wall = this.physics.add.image(500,500, 'wall');
        // wall.setScale(5);
        // wall.setImmovable(true);
        // this.physics.add.collider(this.player, objects, () => {console.log('boom');});
        /////////////////////////////////////this.physics.add.collider(this.player, floor, () => { console.log('boom'); });
        // this.physics.add.collider(this.player, carept, () => {console.log('boom');});
        // this.physics.add.collider(this.player, details, () => {console.log('boom');});

        this.cursors = this.input.keyboard.createCursorKeys();
    }

    update() {

        this.player.setVelocity(0, 0);

        if (this.cursors.right.isUp) {
            this.player.anims.play('walk', false);
        }

        if (this.cursors.left.isDown) {
            this.player.setVelocityX(-20);
        }
        else if (this.cursors.right.isDown) {
            this.player.anims.play('walk', true);
            //this.player.play({ key: 'walk', repeat: 7 });
            this.player.setVelocityX(20);
        }

        if (this.cursors.up.isDown) {
            this.player.setVelocityY(-20);
        }
        else if (this.cursors.down.isDown) {
            this.player.setVelocityY(20);
        }
    }
}

const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
    },
    scene: MyGame
};

const game = new Phaser.Game(config);
